function disp_history() {

  document.getElementById("story_div").style.display = "block";
  document.getElementById("prod_div").style.display = "none";
  document.getElementById("love_div").style.display = "none";
  document.getElementById("restau_div").style.display = "none";
}
function disp_produc() {

  document.getElementById("story_div").style.display = "none";
  document.getElementById("prod_div").style.display = "block";
  document.getElementById("love_div").style.display = "none";
  document.getElementById("restau_div").style.display = "none";
}
function disp_passion() {

  document.getElementById("story_div").style.display = "none";
  document.getElementById("prod_div").style.display = "none";
  document.getElementById("love_div").style.display = "block";
  document.getElementById("restau_div").style.display = "none";
}
function disp_restau() {

  document.getElementById("story_div").style.display = "none";
  document.getElementById("prod_div").style.display = "none";
  document.getElementById("love_div").style.display = "none";
  document.getElementById("restau_div").style.display = "block";
}


function disp_begin() {

  document.getElementById("disp_entree").style.display = "block";
  document.getElementById("disp_plat").style.display = "none";
  document.getElementById("disp_dessert").style.display = "none";
}

function disp_mid() {

  document.getElementById("disp_entree").style.display = "none";
  document.getElementById("disp_plat").style.display = "block";
  document.getElementById("disp_dessert").style.display = "none";
}

function disp_end() {

  document.getElementById("disp_entree").style.display = "none";
  document.getElementById("disp_plat").style.display = "none";
  document.getElementById("disp_dessert").style.display = "block";
}
