<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="contact.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
		integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<title></title>
</head>

<body>
	<div class="dropdown">
		<button style="margin-top: 5%; margin-left: 5%; margin-bottom: 5%;" onclick="myFunction()"
			class="dropbtn">Menu</button>
		<div id="myDropdown" class="dropdown-content">
			<input type="text" placeholder="Search.." id="myInput" onkeyup="filterFunction()">
			<a href="../Accueil/accueil.html">Accueil</a>
			<a href="../Restaurant/restau.html">Restaurant</a>
			<a href="../Carte/carte.html">Carte</a>
			<a href="../Contact/contact.php">Contact</a>	
		</div>
	</div>

	<header>
		<div style="width: 15%;"></div>
		<img src="../assets/contact2.jpg" style="display: inline-block;width: 80%;">
		<div style="width: 15%;"></div>
	</header>

	<div style="height: 100px;"></div>

	<div class="form-container"><div style="width: 20%;"></div>

<div class="container">
	<div class="container-title">
		<h1>CONTACTEZ NOUS !</h1>
		<h2>Une question ? Une demande de réservation ? </h2>
		L'équipe de restaurant La Cote est à votre disposition.
	</div>

<!-- Début formulaire -->
<form action="post_contact.php" class="theme-form-one form-validation" autocomplete="off">
						<div class="row">
							<div class="col-lg-4 col-md-6 col-12"><input type="text" placeholder="Nom *" name="name"></div>
							<div class="col-lg-4 col-md-6 col-12"><input type="email" placeholder="Adresse email *" name="email"></div>
							<div class="col-lg-4 col-12"><input type="text" placeholder="Numéro de téléphone *" name="phone"></div>
							<div class="col-lg-4 col-12">
								<select class="form-control" id="exampleSelect1" name="sub">
									<option>Objet</option>
									<option>Demande d'information</option>
								    <option>Expertise</option>
								    <option>Conseil juridique</option>
								    <option>Formation technique</option>
								    <option>Formation juridique</option>
								    <option>Autre sujet</option>
								</select>
								
							</div>
							<div class="col-lg-8 col-12"><textarea placeholder="Votre message *" name="message"></textarea></div>
						</div>
						<p><i style="margin-left:20px;font-size:13px;">* Champs obligatoires</i></p>
						<button class="theme-button-one">Envoyer</button>
					</form>
				</div> <!-- /.container -->
				<!--Contact Form Validation Markup -->
									<!-- Contact alert -->
									<div class="alert-wrapper" id="alert-success">
										<div id="success">
											<button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
											<div class="wrapper">
												   <p>Votre message a bien été envoyé.</p>
											 </div>
										</div>
									</div> <!-- End of .alert_wrapper -->
									<div class="alert-wrapper" id="alert-error">
										<div id="error">
											   <button class="closeAlert"><i class="fa fa-times" aria-hidden="true"></i></button>
											   <div class="wrapper">
												   <p>Désolé, une erreur est survenue</p>
											</div>
										</div>
									</div> <!-- End of .alert_wrapper -->
<!-- Fin formulaire -->

</div>
	</div>

	<div class="reservation">
		<div id="sideblank"></div>
		<div class="mainpart">
			<!--- <iframe src="https://www.google.fr/maps/d/embed?mid=1DUowNxQtfGd72fXheVilmQE4aAvA-LHE&hl=fr" width="640" height="480"></iframe> --->
			<img src="../assets/contact-1.jpg">
			<div id="topblank"></div>
			<div class="first-part">
				<h1>On attend plus que vous !</h1>
			</div>
			<div class="second-part">
				<div class="reserv">
					<p>Pour réserver une table ou vérifier l’adresse d’un de nos restaurants, c’est là que ça se passe.
					</p>
					<p><a href="../Accueil/accueil.html">Retour accueil</a></p>
				</div>

				<div id="middleblank"></div>

				<div class="contact">
					<p>Une question ? Une demande d’interview ? Des mots d’amour à nous envoyer ?</br>
						<b>Contactez le siège.</b></p>
					<p><i class="fas fa-map-marker-alt"></i> 22 Impasse Charles Fourier</br><span
							style="margin-left: 20px">31200
							Toulouse</span></p>
					<p><i class="fas fa-phone"></i> 05 82 95 10 48</p>
				</div>
			</div>
		</div>
	</div>
</body>


<script>
	/* When the user clicks on the button,
	toggle between hiding and showing the dropdown content */
	function myFunction() {
		document.getElementById("myDropdown").classList.toggle("show");
	}

	function filterFunction() {
		var input, filter, ul, li, a, i;
		input = document.getElementById("myInput");
		filter = input.value.toUpperCase();
		div = document.getElementById("myDropdown");
		a = div.getElementsByTagName("a");
		for (i = 0; i < a.length; i++) {
			txtValue = a[i].textContent || a[i].innerText;
			if (txtValue.toUpperCase().indexOf(filter) > -1) {
				a[i].style.display = "";
			} else {
				a[i].style.display = "none";
			}
		}
	}
</script>

</html>